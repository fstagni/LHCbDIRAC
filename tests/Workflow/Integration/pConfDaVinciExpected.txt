from ProdConf import ProdConf

ProdConf(
  NOfEvents=-1,
  DDDBTag='Sim08-20130503-1',
  CondDBTag='Sim08-20130503-1-vc-mu100',
  AppVersion='v32r2p1',
  XMLSummaryFile='summaryDaVinci_00012345_00006789_5.xml',
  Application='DaVinci',
  OutputFilePrefix='00012345_00006789_5',
  XMLFileCatalog='pool_xml_catalog.xml',
  InputFiles=['LFN:00012345_00006789_4.dst'],
  OutputFileTypes=['allstreams.dst'],
)