from ProdConf import ProdConf

ProdConf(
  NOfEvents=-1,
  AppVersion='v34r2',
  XMLSummaryFile='summaryLHCb_00012345_00006789_1.xml',
  Application='LHCb',
  OutputFilePrefix='00012345_00006789_1',
  XMLFileCatalog='pool_xml_catalog.xml',
  InputFiles=['LFN:/lhcb/LHCb/Collision12/FMDST/00020751/0000/00020751_00000280_1.fmdst', 'LFN:/lhcb/LHCb/Collision12/FMDST/00020751/0000/00020751_00000357_1.fmdst'],
  OutputFileTypes=['fmdst'],
)