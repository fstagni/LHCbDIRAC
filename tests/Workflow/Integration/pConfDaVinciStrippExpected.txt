from ProdConf import ProdConf

ProdConf(
  NOfEvents=500,
  DDDBTag='dddb-20130111',
  CondDBTag='cond-20130114',
  AppVersion='v32r2p11',
  OptionFormat='Stripping',
  XMLSummaryFile='summaryDaVinci_00012345_00006789_1.xml',
  Application='DaVinci',
  OutputFilePrefix='00012345_00006789_1',
  XMLFileCatalog='pool_xml_catalog.xml',
  InputFiles=['LFN:/lhcb/LHCb/Collision11/FULL.DST/00022719/0009/00022719_00095865_1.full.dst', 'LFN:/lhcb/LHCb/Collision11/FULL.DST/00022719/0010/00022719_00102702_1.full.dst'],
  OutputFileTypes=['bhadron.mdst', 'bhadroncompleteevent.dst', 'calibration.dst', 'charm.mdst', 'charmcompleteevent.dst', 'dimuon.dst', 'ew.dst', 'leptonic.mdst', 'semileptonic.dst'],
)