==============
System Testing
==============

System ( FULL CHAIN ) Testing is evaluates the general status of the SW deployed. 

At least 2 test files are here. Get them via:
wget http://svnweb.cern.ch/world/wsvn/dirac/LHCbDIRAC/tests/trunk/LHCbDIRAC/tests/System/Client/client_test.csh
wget http://svnweb.cern.ch/world/wsvn/dirac/LHCbDIRAC/tests/trunk/LHCbDIRAC/tests/System/GridTestSubmission/testUserJobs.py

and again simply run them (csh and python) 
 