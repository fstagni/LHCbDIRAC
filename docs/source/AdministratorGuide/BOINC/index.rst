=======================================
DIRAC on BOINC
=======================================

.. toctree::
   :maxdepth: 2

   design.rst
   configuration.rst
   gatewaySetup.rst
   gatewayInstallation.rst
   boincImage.rst
   testBoinc.rst
   todo.rst
